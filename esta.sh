#!/bin/bash

#dar argumentos para la salida del error
#  [$# -ne 1 ] nos dice que no puede haber menos de un argumento

if [ "$#" -ne 1 ]; then 
	echo "el numero de arguementos no es correcto"
	echo "sintaxis: $0 nombre-usuario"
	exit 1
fi 

#damos la variable al usuario 
 usuario=$1

#comprobar que esta conectado

if who | grep -q "$usuario"; then #ver la lista de usuarios  
	echo "El usuario "$1" esta conectado ahora"
else
	echo "El usuario "$1" no esta conectado"
fi
