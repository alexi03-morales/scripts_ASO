#!/bin/bash

	read -p "ingrese el nombre del usuario: " usuario


	#Verificamos si el usuario existe

	if id "$usuario" >/dev/null 2>&1; then

	#Mostrar la información de procesos para el usuario dado

	echo "Procesos para el usuario $usuario:"
    	procesos_usuario=$(ps -U $usuario)
    	echo "$procesos_usuario"
	else

	#Imprimir un mensaje de error si el usuario no existe

	echo "Error: El usuario $usuario no existe."
    	exit 1
	fi
