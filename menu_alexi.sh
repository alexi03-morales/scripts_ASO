#!/bin/bash

#depuraciion del codigo 
#set -x 

fecha=$(date +"%Y-%m-%d")
hora=$(date +"%H:%M:%S")

echo "HOY ES $fecha Y SON LAS $hora GRACIAS POR ESTAR AQUI!:)"
echo "BIENVENIDO AL MENU DE ALEXI"
 
function crear_usuarios() {

	#pasar el nombre del uusario a crear como parametro: 

	read -p "Ingrese el nombre de usuario a crear: " nuevo_user

	#verificamos que nuestro usuario no existe en el sistema

	if id "$nuevo_user" &>/dev/null; then 

	echo "Usuario $nuevo_user ya existe intenta de nuevo."

	else
	sudo adduser   $nuevo_user
	echo "Usuario $nuevo_user creado correctamente."

	fi

}

function crear_directorios() {
	#asignamos las opciones del submenu 
	while true; do
        echo "Menú:"
        echo "1. Crear de manera básica."
        echo "2. Crear desde fichero externo, recuerda decir el fichero."
        echo "3. Volver al menu principal"

	read -p "Selecciona una opción: " opcion
	case $opcion in

	#pasamos el nomnbre del direcotrio como parametro: 

	1)
	read -p "Ingrese el nombre del directorio a crear: " nuevo_dir

	#comprobamos que no existe

	if [ -d $nuevo_dir ]; then 
        echo "El directorio $nuevo_dir ya existe."
	else 
        sudo mkdir $nuevo_dir
        echo "Directorio $nuevo_directorio creado correctamente."

	fi ;;

	2)
	#pasamos el nombre del archivo como parametro
	read -p " ingrese el nombre del archivo creador: " archivo

	if [ -f $archivo ]; then 

	#Leemos cada  línea del archivo para luego crear los directorios 

	while IFS= read -r directorio; do

	#Verificaos  si nuestro  directorio ya existe

	if [ -d "$directorio" ]; then
        echo "El directorio $directorio ya existe. Omítelo."
	else
        # Crear el directorio
        sudo mkdir "$directorio"

	echo "Directorio $directorio creado exitosamente."
    	fi
	done < "$archivo" 
	else 
        echo "El archivo creador $fichero no existe."
	fi ;;


3) 	break ;;

*)	 "Selecciona la opcion correcta." ;; 
        esac
	done
 

}

function ver_procesos_usuario() {
	#damos el nombre del usuario como parametro 

	read -p "ingrese el nombre del usuario: " usuario


        #Verificamos si el usuario existe

        if id "$usuario" >/dev/null 2>&1; then

        #Mostrar la información de procesos para el usuario dado

        echo "Procesos para el usuario $usuario:"
        procesos_usuario=$(ps -U $usuario)
        echo "$procesos_usuario"
        else

        #Imprimir un mensaje de error si el usuario no existe

        echo "Error: El usuario $usuario no existe."
        exit 1
        fi
}

function crear_ficheros() {
	#pasamos como parametro el nombre del fichero

	read -p "Ingrese el nombre del fichero a crear: " nuevo_fichero

	#comprobamos que nuestro fichero no existe previamente: 

	if [ -f $nuevo_fichero ]; then 
	echo "El fichero $nuevo_fichero ya existe."
	else 

	#creamos el fichero una vez comprobada su existencia

	sudo cat>>$nuevo_fichero
	echo "Fichero $nuevo_fichero creado correctamente."
	fi
}

function listar_contenidos() {

	while true; do
	echo "Menú:"
    	echo "1. Listar contenido de un archivo o directorio"
    	echo "2. Volver a menu principal"

    	read -p "Selecciona una opción: " opcion

    	case $opcion in
        1)
	#pasamos el nombre del fichero o directorio como parametro: 

	read -p "Ingrese el nombre del archivo o directorio: " nombre_contenido

	#establecemos que si nuestro fichero o directorio existe: 

            if [ -d "$nombre_contenido" ]; then

	#si es un directorio hacemos un ls -l para ver su contenido y permisos
                echo -e "Contenido de $nombre_contenido:\n"
                ls -l "$nombre_contenido"

	#y si es un fichero hacemos un cat del contenido: 
	elif [ -f "$nombre_contenido" ]; then 
		echo -e "Contenido de $nombre_contenido:\n"
		cat $nombre_contenido
            else
                echo "Error: El archivo o directorio $nombre_contenido no existe."
            fi
            ;;
        2)
            echo "Volviendo al menu principal."
            break ;;
        *)
            echo "Opción inválida. Por favor, selecciona una opción válida." ;;
    esac
done

} 

function ping_dispositivo() {

	#Solicitamos la direccion ip o la url para realizar el ping pasada como parametro: 

	read -p "Ingrese el host o la URL para hacer ping: " host

	#Realizar el ping
	ping -c 4 $host

	#Comprobar el estado de salida del comando ping

	if [ $? -eq 0 ]; then
    	echo "Ping exitoso hacia $host."
	else

	echo "Ping fallido hacia $host realiza la prueba de nuevo. "
	fi
} 


function apagar_encender(){

	#establecemos como apagar o reiniciar el ordenador de forma interactiva

	echo "Menú:"
	echo "1. Apagar el ordenador"
	echo "2. Reiniciar el ordenador"
	echo "3. Cancelar"

	read -p "Seleccione una opción (1/2/3): " opcion

	case $opcion in
   	 1)
	#apagar el ordenaor ahora mismo 
        sudo shutdown -h now
        ;;
   	 2)
	#reiniciar el ordenador
        sudo reboot
        ;;
    	3)
        echo "Operación cancelada."
        ;;
    	*)
        echo "Opción no válida. Inténtelo de nuevo."
        ;;
	esac
}

function configuracion_de_red() {

	#muestra nuestra configuración de red

	echo "Configuración de red:"

	#Mostrar información de interfaces de nuestra  red

echo "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	echo "Interfaces de red: "
	ifconfig -a


	#Mostrar información de nuestra tabla de enrutamiento

echo "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	echo -e "\nTabla de enrutamiento:"
	route -n

	#Mostrar información de los servidores DNS

echo "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	echo -e "\nConfiguración de DNS:"
	cat /etc/resolv.conf

	#Mostrar información de la puerta de enlace predeterminada
echo "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	echo -e "\nPuerta de enlace predeterminada:"
	ip route | grep default

	ip route | grep def

echo "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"

}

function cambiar_permisos() {

	read -p "Ingrese la ruta del archivo o directorio: " ruta

	#Verificar si la ubicacion del archivo es válida

	if [ -e "$ruta" ]; then
    	#Mostrar los permisos actuales
   	 echo "Permisos actuales de $ruta:"
    	ls -la "$ruta"


	#Solicitar al usuario que ingrese los nuevos permisos

	read -p "Ingrese los nuevos permisos (por ejemplo, 755): " nuevos_permisos

	#Modificar los permisos

	sudo chmod "$nuevos_permisos" "$ruta"

	#Mostrar los nuesvos permisos:

	echo -e "\nNuevos permisos de $ruta:"

	ls -la "$ruta"

	else
    	echo "La ruta especificada no es válida."
	fi

}

	while true; do
	echo "OPCIONES:"
	echo "1. Crear usuarios"
	echo "2. Crear directorios"
	echo "3. Consultar procesos del usuario actual"
	echo "4. Crear ficheros"
	echo "5. Listar contenidos"
	echo "6. Ping dispositivo"
	echo "7. Apagar o reiniciar"
	echo "8. Configuracion de red"
	echo "9. Cambiar permisos a directorios o ficheros"
	echo "0. Salir"

	read -p "Selecciona una opción: " opcion

    	case $opcion in
        1) crear_usuarios ;;
        2) crear_directorios ;;
        3) ver_procesos_usuario ;;
        4) crear_ficheros ;;
	5) listar_contenidos ;;
        6) ping_dispositivo ;;
	7) apagar_encender ;;
	8) configuracion_de_red ;;
	9) cambiar_permisos ;;
	0)
            echo "Gracias por usar nuestro service"
            exit ;;
        *)
            echo "Selecciona una opcion del menu tontito:)." ;;
    	esac

done
